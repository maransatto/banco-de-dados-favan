#Remove database
use cadastro;
drop database library;

# Criar database livraria
create database library;

use library;

# Criar tabela livros (nome, preco)
create table book (
			id_book int primary key not null auto_increment,
			name_book varchar(80),
			price float
);

#Renomear tabela para livros
alter table book rename books;

# Inserir registros de livro (java, 95, css, 55)
insert into books (
			name_book,
            price
) values ('java', 95.00), ('css', 55.00);

select * from books;

# Atualizar preços dos livros (java, 110, css 80.00)
update books set price = 110.00 where name_book = 'java';
update books set price = 80.00 where name_book = 'css';

# Selecionar apenas o livro de java
select * from books where name_book = 'java';

# Deletar livro "java"
delete from books where name_book = 'java';

# Criar tabela de autories com ID e Nome
create table authors (
				id int primary key auto_increment,
                name_author varchar(80)
                );